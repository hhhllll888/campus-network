# 自己写的一个校园网登陆跳过
### 安装方法
#### 谷歌浏览器
[安装链接](https://gitee.com/z4042346/campus-network/raw/master/tampermonkey.crx)
谷歌浏览器点击这个链接安装，可能需要在扩展界面打开开发者模式
然后在管理界面
![](https://pic.imgdb.cn/item/6199ae732ab3f51d91e27a1e.png)
从url处安装
将
~~~
https://gitee.com/z4042346/campus-network/raw/master/%E6%A0%A1%E5%9B%AD%E7%BD%91%E7%99%BB%E5%BD%95%E8%87%AA%E5%8A%A8%E7%82%B9%E5%87%BB.js
~~~
复制到里面即可安装
#### 火狐浏览器
[点我](https://addons.mozilla.org/zh-CN/firefox/addon/tampermonkey/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search)
点这个链接直接进火狐扩展商店安装，让后同上
### 注意
- 建议装在自己的默认浏览器中，如果有多个浏览器必须保证是默认浏览器，怎么看自己的默认浏览器自己百度。
- 安装后浏览器必须是记住密码状态，否则一直弹窗，必须在tampermokey中关闭脚本。
- 360浏览器、搜狗浏览器、edge浏览器都是谷歌内核，按照谷歌浏览器的安装方法来